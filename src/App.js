import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import SignIn from "./ui/auth/login";
import Table from "./pages/table";

function BasicExample() {
  return (
          <Router>
              <Route exact path="/" component={SignIn} />
              <Route path="/login" component={Table} />
          </Router>
  );
}

export default BasicExample;