import React from "react";

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';

class Login extends React.Component {

    state = {email: '', password: ''};
    componentDidMount() {
     // console.log(this.state.email.value);

    }

    handleChange = (event) => {
      const value = event.target.value;
      const name = event.target.name;
      this.setState({
        [name]: value,
        checkbox: event.target.checked
      });
    }

    handleSubmit = (event) => {
      event.preventDefault();

      console.log(this.state.email);

      console.log(this.state.password);
      console.log(this.state.checkbox);
      //const data = new FormData(event.target);
      /*fetch('/api/form-submit-url', {
        method: 'POST',
        body: data,
      });*/
    }

    render() {
      return (
      <React.Fragment >
              <form onSubmit={this.handleSubmit}>
          <FormControl margin="normal" required fullWidth>
          <InputLabel htmlFor="email">Email Address</InputLabel>
          <Input type="email" id="email" name="email" autoComplete="email" value={this.state.email} onChange={this.handleChange} autoFocus />
        </FormControl>
        <FormControl margin="normal" required fullWidth>
          <InputLabel htmlFor="password">Password</InputLabel>
          <Input name="password" type="password" id="password" value={this.state.password} onChange={this.handleChange} autoComplete="current-password" />
        </FormControl>
        <FormControlLabel
          control={<Checkbox checked={this.state.checkbox} onChange={this.handleChange} color="primary" />}
          label="Remember me"
        />
          <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          
        >
          Sign in
        </Button>
        </form>
    </React.Fragment>
    );
    }
}
export default Login;
